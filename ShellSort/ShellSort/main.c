#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h> 

#define MAX_ARRAY_SIZE 100000000
#define SOURCE_FILE "source.txt"
#define RESULT_FILE "answer.txt"

// file work
void randomArrayFileGenerate(int* link, int arrSize);
void fillArrayFromFile(int* link);
void fillAnswerFile(int* link, int arrSize);

// basic
void menu();
void printArray(int* link, int arrSize, int forcePrint);
void shellSort(int* link, int length, int reverse); // 0 - normal, 1 - reverse
void shellSortNormal(int* link, int length);
void shellSortReverse(int* link, int length);

int* arr = 0;

int main()
{
	menu();

	printf("Press 'Enter' to exit\n");
	getchar();
	
	return 0;
}

#pragma region FileWork
void randomArrayFileGenerate(int* link, int arrSize)
{
	FILE* isxodnik = fopen(SOURCE_FILE, "w+");
	if (isxodnik == NULL)
	{
		printf("File can't be open or doesn't exist");
		return;
	}

	printf("\nStart fill %s...\n", SOURCE_FILE);

	srand(time(NULL));
	for (int i = 0; i < arrSize; i++)
	{
		fprintf(isxodnik, "%d ", (10 + (rand() % 90)));
	}

	printf("End fill %s\n", SOURCE_FILE);

	fclose(isxodnik);
}

void fillArrayFromFile(int* link)
{
	FILE* isxodnik = fopen(SOURCE_FILE, "r+");
	if (isxodnik == NULL)
	{
		printf("File can't be open or doesn't exist");
		return;
	}

	printf("\nStart fill array from %s...\n", SOURCE_FILE);
	int b = 0;
	for (int i = 0; (fscanf(isxodnik, "%d", &b) != EOF); i++)
	{
		*(link + i) = b;
	}

	printf("End fill array from %s\n", SOURCE_FILE);

	fclose(isxodnik);
}

void fillAnswerFile(int* link, int arrSize)
{
	FILE* rezult = fopen(RESULT_FILE, "w+");
	if (rezult == NULL)
	{
		printf("File can't be open or doesn't exist");
		return;
	}

	printf("\nStart fill %s...\n", RESULT_FILE);

	for (int i = 0; i < arrSize; i++)
	{
		fprintf(rezult, "%d ", *(link + i));
	}

	printf("End fill %s\n", RESULT_FILE);

	fclose(rezult);
}
#pragma endregion FileWork

#pragma region Basic
void printArray(int* link, int arrSize, int forcePrint)
{
	if (forcePrint == 0 || arrSize > 35)
		return;

	printf("Array: ");

	for (int i = 0; i < arrSize; i++)
	{
		printf("%d ", *(link + i));
	}
	printf("\n");
}

void shellSort(int* link, int length, int reverse)
{
	if (reverse == 0)
	{
		shellSortNormal(link, length);
		return;
	}

	shellSortReverse(link, length);
}

void shellSortNormal(int* link, int length)
{	
	printf("\n===========================\n");
	printf("Start sort....\n");

	double time_spent = 0.0;
	clock_t begin = clock();

	int interval = length / 2;
	while (interval > 0)
	{
		for (int i = interval; i < length; i++)
		{
			int temp = *(link + i);
			int j = i;
			while (j >= interval && *(link + j - interval) > temp)
			{
				*(link + j) = *(link + j - interval);
				j -= interval;
			}
			*(link + j) = temp;
		}
		interval /= 2;
	}
	clock_t end = clock();
	time_spent += (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Sort end. Time spent %f seconds\n", time_spent);
	printf("===========================\n");
}

void shellSortReverse(int* link, int length)
{
	double time_spent = 0.0;
	printf("\n===========================\n");
	printf("Start sort....\n");

	clock_t begin = clock();

	int interval = length / 2;
	while (interval > 0)
	{
		for (int i = interval; i < length; i++)
		{
			int temp = *(link + i);
			int j = i;
			while (j >= interval && *(link + j - interval) < temp)
			{
				*(link + j) = *(link + j - interval);
				j -= interval;
			}
			*(link + j) = temp;
		}
		interval /= 2;
	}
	clock_t end = clock();
	time_spent += (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Sort end. Time spent %f seconds\n", time_spent);
	printf("===========================\n");
}

void menu()
{
	int n = 0;
	int rotation = 0;

	printf("Insert array size (>= 1): ");
	scanf("%d", &n);
	getchar();
	if (n < 1)
		n = 1;
	else if (n > MAX_ARRAY_SIZE)
		n = MAX_ARRAY_SIZE;
	printf("Array size = %d\n", n);

	printf("Sort type (Normal - 0, Reverse - 1): ");
	scanf("%d", &rotation);

	// ��������� �������� ���� ���������� �������
	randomArrayFileGenerate(arr, n);

	// �������� ������ ��� ������ � ��������� ��� �� ����� � ��������� �������
	arr = (int*)malloc(n * sizeof(int));
	fillArrayFromFile(arr);
	printArray(arr, n, 0);

	// ����������
	shellSort(arr, n, rotation);

	// ��������� ���� � ������������ ��������������� ��������
	fillAnswerFile(arr, n);
	printArray(arr, n, 0);

	printf("\n===========================\n");
	printf("Array saved in - %s\n", SOURCE_FILE);
	printf("Sorted array saved in - %s\n", RESULT_FILE);
	printf("===========================\n\n");
}
#pragma endregion Basic
